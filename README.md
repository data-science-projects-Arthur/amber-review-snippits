**Amber Review Snippets**

A collection of scripts and analyses that were contributed to the amber review
(a review into changes in the ambulance system in wales and its effects on patient
safety.)

This included:
1. GIS work to view the origins of ambulance calls(folium).
2. Unsupervised learning for anomaly detection on ambulance calls. 
3. Simple r-Shiny dashboard to create a histogram of wait time for each ambulance call.